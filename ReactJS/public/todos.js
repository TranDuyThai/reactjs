class Footer extends React.Component {
  constructor(props) {
    super(props);
  
    this.onClickFilter = this.onClickFilter.bind(this);
    this.removeAllCompleted = this.removeAllCompleted.bind(this);
  }
  
  onClickFilter(e) {
    this.refs.All.className = "";
    this.refs.Active.className = "";
    this.refs.Completed.className = "";
    e.target.className = "selected";
    if (this.refs.All.className === "selected") {
      this.props.filter(null);
    }
    else if (this.refs.Active.className === "selected") {
      this.props.filter(false);
    }
    else if (this.refs.Completed.className === "selected") {
      this.props.filter(true);
    }
  }
  
  removeAllCompleted() {
    this.props.removeCompleted();
  }
  
  render() {
    return (
      
      <footer className="footer">
					<span className="todo-count">
            <strong>{this.props.activeItem} items left</strong>
					</span>
        <ul className="filters">
          <li>
            <a ref="All" value="" href="#/" onClick={this.onClickFilter} className="selected">All</a>
          </li>
          <li>
            <a ref="Active" value="false" href="#/active" onClick={this.onClickFilter}>Active</a>
          </li>
          <li>
            <a ref="Completed" value="true" href="#/completed" onClick={this.onClickFilter}>Completed</a>
          </li>
        </ul>
        <button className="clear-completed" onClick={this.removeAllCompleted}>Clear completed</button>
      </footer>
    );
  }
}

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.onClickClose = this.onClickClose.bind(this);
    this.onClickDone = this.onClickDone.bind(this);
  }
  componentDidMount() {
    var todoClass = this.props.item.completed ? "completed" : "";
    this.refs.finished.className = todoClass;
  }
  onClickClose() {
    this.props.removeItem(this.props.item.index);
  }
  onClickDone() {
    if (this.refs.check.checked === true) {
      this.refs.finished.className = "completed";
    } else {
      this.refs.finished.className = "";
    }
    var checked = this.refs.check.checked;
    this.componentDidMount();
    this.props.markTodoDone(this.props.item.index, checked);
    
  }
  
  render() {
    var todoClass = this.props.item.completed ? "completed" : "";
    
    return (
      <li ref="finished" className={todoClass}>
        <div className="view">
          <input ref="check" className="toggle" type="checkbox" checked={this.props.item.completed}
                 onChange={this.onClickDone}/>
          <label>{this.props.item.value}</label>
          <button className="destroy" onClick={this.onClickClose}></button>
        </div>
        <form>
          <input className="edit"/>
        </form>
      </li>
    );
  }
}

class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.onClickCheck = this.onClickCheck.bind(this);
  }
  
  onClickCheck() {
    this.props.checkAll(this.refs.check.checked);
  }
  
  render() {
    var items;
    if (this.props.status === null) {
      items = this.props.items.map((item, index) => {
        return (
          <TodoItem key={index.toString()} item={item} index = {index} removeItem={this.props.removeItem}
                    markTodoDone={this.props.markTodoDone}/>
        );
      });
    }
    else {
      items = this.props.items.filter(data => data.completed == this.props.status).map((item, index) => {
        return (
          <TodoItem key={index.toString()} item={item} index={index} removeItem={this.props.removeItem}
                    markTodoDone={this.props.markTodoDone}/>
        );
      });
    }
    
    return (
      <section className="main">
        <input ref="check" className="toggle-all" type="checkbox" onClick={this.onClickCheck}/>
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
          {
            items
          }
        </ul>
      </section>
    );
  }
}

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.todoList,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }
  
  componentDidMount() {
    this.refs.newItem.focus();
  }
  onSubmit(event) {
    event.preventDefault();
    var newItemValue = this.refs.newItem.value;
    if (newItemValue) {
      this.props.addItem(newItemValue);
      this.refs.form.reset();
    }
  }
  
  render() {
    return (
      <div>
        <form ref="form" className="todo-form" onSubmit={this.onSubmit}>
          <input className="new-todo" placeholder="What needs to be done?" ref="newItem"/>
        </form>
      </div>
    );
  }
}

var todoList = [];
 todoList.push({index: 1, value: "learn react", completed: false});
todoList.push({index: 2, value: "Go shopping", completed: true});
todoList.push({index: 3, value: "buy flowers", completed: true});
todoList.push({index: 4, value: "do exercise", completed: false});

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.markTodoDone = this.markTodoDone.bind(this);
    this.removeCompleted = this.removeCompleted.bind(this);
    this.countActive = this.countActive.bind(this);
    this.checkAll = this.checkAll.bind(this);
    this.filterItems = this.filterItems.bind(this);
    
    this.state = {
      todoList: todoList,
      filterStatus: null,
      activeItem: 0,
    };
  }
  
  componentDidMount() {
    this.countActive();
  }
  
  addItem(newItemValue) {
    var newItem = {
      index: todoList.length + 1,
      value: newItemValue,
      completed: false
    }
    this.setState({todoList: todoList.push(newItem)});
    this.state.todoList = todoList;
    this.countActive();
  }
  
  removeItem(itemIndex) {
    var index = todoList.findIndex(item => item.index === itemIndex);
    todoList.splice(index, 1);
    this.setState({todoList: todoList});
    this.state.todoList = todoList;
    this.countActive();
  }
  
  markTodoDone(itemIndex, checked) {
    var index = todoList.findIndex(item => item.index === itemIndex);
    todoList[index].completed = checked;
    this.setState({todoList: todoList });
    this.state.todoList = todoList;
    this.countActive();
  }
  
  removeCompleted() {
    var listId = [];
    for (let item of this.state.todoList)
      if (item.completed === true) {
        listId.push(item.index)
      }
    for (let i of listId){
      var index = todoList.findIndex(item => item.index === i);
      todoList.splice(index, 1);
    }
    this.setState({todoList:todoList});
    this.state.todoList = todoList;
  }
  
  checkAll(checked) {
    for (var i = 0; i < this.state.todoList.length; i++) {
      todoList[i].completed = checked;
    }
    this.setState({todoList: todoList});
    this.state.todoList = todoList;
    this.countActive();
  }
  
  countActive() {
    var count = 0;
    for (var i = 0; i < this.state.todoList.length; i++) {
      if (todoList[i].completed === false) {
        count++;
      }
    }
    this.setState({activeItem: count});
    this.state.activeItem = count;
  }
  
  filterItems(status) {
    this.setState({filterStatus: status});
    this.state.filterStatus = status;
    console.log(this.state);
  }
  
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <Input addItem={this.addItem}/>
        </header>
        <TodoList status={this.state.filterStatus} items={this.props.List} removeItem={this.removeItem}
                  markTodoDone={this.markTodoDone} checkAll={this.checkAll}/>
        <Footer activeItem={this.state.activeItem} removeCompleted={this.removeCompleted} filter={this.filterItems}/>
      </section>
    )
  }
}

ReactDOM.render(<App List={todoList}/>, document.getElementById('root'));
