/*import React from 'react';
import ReactDOM from 'react-dom';*/

class Filter extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      status:'',
      onCheck:'',
    }
  }
  render(){
    return(
      <ul className="filters">
        <li>
          <a ng-class="{selected: status == ''} " href="#/">All</a>
        </li>
        <li>
          <a ng-class="{selected: status == 'active'}" href="#/active">Active</a>
        </li>
        <li>
          <a ng-class="{selected: status == 'completed'}" href="#/completed" className="selected">Completed</a>
        </li>
      </ul>
    );
  }
}

class TodoItem extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
    };
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange({target}){
    if (target.checked){
      this.refs.finish.className="completed";
    } else {
      this.refs.finish.className ="";
    }
  }
  
  deleteHandler(i, e) {
    e.preventDefault();
    this.props.onDelete(this.props.todoList[i].id);
  };
  
  render(){
    return(
      <li ref="finish" className={this.props.completed}>
        <div className="view">
          <input className="toggle" type="checkbox" onClick={this.handleChange} defaultChecked={this.props.onCheck}/>
          <label>{this.props.text}</label>
          <button className="destroy" onClick={() => this.deleteHandler(i)}></button>
        </div>
        <form>
          <input className="edit"/>
        </form>
      </li>
    );
  }
}

class TodoList extends  React.Component{
  constructor(props) {
    super(props);
    this.state = {
      text:'',
      completed:'',
      onCheck:'',
    }
    this.newItem = this.newItem.bind(this);
  }
  
  newItem(item) {
    return <TodoItem key={item.key} text={item.text} completed={item.completed} onCheck={item.onCheck} />
  }
  onDelete(id) {
    deleteBlogPost(id)
      .then((data) => {
        let blogPosts = this.state.blogPosts.filter((post) => {
          return id !== post.id;
        });
        
        this.setState(state => {
          state.blogPosts = blogPosts;
          return state;
        });
      })
      .catch((err) => {
        console.error('err', err);
      });
  }
  render(){
    var todoList = this.props.todoList;
    var listItems = todoList.map(this.newItem);
    return(
      <section className="main">
        <input className="toggle-all" type="checkbox" />
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
          {
            listItems
          }
        </ul>
      </section>
    );
  }
}

class Input extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.todoList,
    };
    
    this.addTodo = this.addTodo.bind(this);
  }
  addTodo(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        completed: '',
        onCheck: false,
        key: Date.now(),
      }
      
      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem)
        };
      });
      console.log(this.state.items);
      this._inputElement.value = "";
    }
   
    e.preventDefault();
  }
  render(){
    return(
      <div>
        <form className="todo-form" onSubmit={this.addTodo}>
          <input className="new-todo" placeholder="What needs to be done?" ref={(a) => this._inputElement = a}/>
        </form>
        <TodoList todoList={this.state.items} />
      </div>
    );
  }
}

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      todoList: [],
      filterStatus:'',
    }
  }
  
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <Input todoList={this.state.todoList}/>
        </header>
        <footer className="footer" >
					<span className="todo-count"><strong className="ng-binding">0</strong>
						<ng-pluralize count="remainingCount" when="{ one: 'item left', other: 'items left' }">items left</ng-pluralize>
					</span>
          <Filter/>
          <button className="clear-completed" >Clear completed</button>
        </footer>
      </section>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));
